<?php
  $post_data = json_decode(file_get_contents('php://input'));
  $conn = mysqli_connect("localhost", "madchick_user", "mc-user-pw", "madchick_database");
  $res = mysqli_query($conn, "SELECT * FROM visitors_list WHERE phone=$post_data->number");
  if(mysqli_num_rows($res) == 0){
    //New user, add it...
    mysqli_query($conn, "INSERT INTO visitors_list (firstname, lastname, phone, visits, allowedVisits) VALUES ('$post_data->firstname','$post_data->lastname','$post_data->number',1,3)");
    $number = mysqli_insert_id($conn);
    $ret = Array(
      "number" => $number,
    );
    echo json_encode($ret);
  } else {
    //Already exists, check times
    $visitor_data = mysqli_fetch_assoc($res);
    if($visitor_data['visits'] >= $visitor_data['allowedVisits']){
      echo "LIMIT_ERROR";
    } else {
      $firstname = $visitor_data['firstname'];
      $lastname = $visitor_data['lastname'];
      $newValue = (int)$visitor_data['visits'] + 1;
      $allowedVisits = $visitor_data['allowedVisits'];
      $phone = $visitor_data['phone'];
      mysqli_query($conn, "DELETE FROM visitors_list WHERE phone='$phone'");
      mysqli_query($conn, "INSERT INTO visitors_list (firstname, lastname, phone, visits, allowedVisits) VALUES ('$firstname','$lastname','$phone','$newValue','$allowedVisits')");
      $number = mysqli_insert_id($conn);
      $ret = Array(
        "number" => $number,
      );
      echo json_encode($ret);
    }

  }
  mysqli_close($conn);
?>
