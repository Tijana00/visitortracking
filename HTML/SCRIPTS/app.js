var app = angular.module('vtApp',['ngRoute']);

app.config(function($routeProvider, $locationProvider) {
    $routeProvider
    .when('/home', {
      templateUrl: 'HTML/TEMPLATES/home.html',
      controller: 'homeController'
    })
    .when('/list', {
      templateUrl: 'HTML/TEMPLATES/list.html',
      controller: 'listController'
    })
    .when('/apply', {
      templateUrl: 'HTML/TEMPLATES/apply.html',
      controller: 'applyController'
    })
    .when('/monitoring', {
      templateUrl: 'HTML/TEMPLATES/monitoring.html',
      controller: 'monitoringController'
    })
    .otherwise({redirectTo: "/home"});
    $locationProvider.html5Mode(true);
});

app.controller("vtController",function ($scope,$location) {
  $scope.main = {
    fullscreen: 0,
    currentRoute: function () {
      return $location.$$path;
    },
    goBack: function () {
      window.history.back();
    },
    changeMode: function () {
      if(window.innerWidth == screen.width && window.innerHeight == screen.height) {
        document.exitFullscreen();
      } else {
        document.body.requestFullscreen();
      }
    }
  }
});

app.controller("homeController", function ($scope) {

});

app.controller("listController", function ($scope) {

});

app.controller("applyController", function ($scope,$http) {
  $scope.apply = {
    firstname: "",
    lastname: "",
    number: "",
    error: null,
    success: null,
    filterInput: function () {this.number = this.number.replace(/[^0-9]+/g, '');},
    callError: function (e) { this.error = e; },
    removeError: function () { this.error = null; },
    callSuccess: function (r) { this.success = r; },
    removeSuccess: function () { this.success = null; },
    process: function () {
      var vm = this;
      if(vm.firstname.replace(" ","") != "" && vm.lastname.replace(" ","") != "" && vm.number.replace(" ","") != ""){
        $http({
          url:"HTML/ENDPOINTS/apply.php",
          method: "POST",
          data: {
            firstname: vm.firstname,
            lastname: vm.lastname,
            number: vm.number,
          }
        }).then(function (r) {
          if(r.data == "LIMIT_ERROR"){
            vm.callError({
              status: "Limit Error",
              statusText: "You have exceeded the maximum visiting limit."
            });
          }
          else {
            vm.callSuccess(r.data);
            vm.firstname = "";
            vm.lastname = "";
            vm.number = "";
          }
        },function (e) {
          vm.callError(e);
        });
      }
      else {
        vm.callError({
          status: "Credentials Error",
          statusText: "Please fill in all fileds."
        });
      }
    }
  }
});

app.controller("monitoringController", function ($scope) {

});
